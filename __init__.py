#
# Show your flag!
# Switch flags with petals II and VIII
#
# (sorry for the hacky code :)
# Douglas 2023-08
#

import st3m.run, random

from st3m.application import Application, ApplicationContext
from st3m.input import InputController, InputState
from ctx import Context
import leds

class Flag(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.t = 0
        self.pos = 0
        self.last_led_update = 0

        # colors taken from https://www.kapwing.com/resources/official-pride-colors-2021-exact-color-codes-for-15-pride-flags/
        self.colors = [
                # classic rainbow flag
                [[0xe5/256, 0, 0],
                 [0xff/256, 0x8d/256, 0],
                 [0xff/256, 0xee/256, 0],
                 [0x02/256, 0x81/256, 0x21/256],
                 [0x00/256, 0x4c/256, 0xff/256],
                 [0x77/256, 0x00/256, 0x88/256],
                 ],
                # transgender flag
                [[0x5b/256, 0xcf/256, 0xfb/256],
                 [0xf5/256, 0xab/256, 0xb9/256],
                 [0xff/256, 0xff/256, 0xff/256],
                 [0xf5/256, 0xab/256, 0xb9/256],
                 [0x5b/256, 0xcf/256, 0xfb/256],
                 [0,0,0],
                 ],
                # bisexual flag
                [[0xd6/256, 0x02/256, 0x70/256],
                  [0xd6/256, 0x02/256, 0x70/256],
                  [0x9b/256, 0x4f/256, 0x96/256],
                  [0x00/256, 0x38/256, 0xa8/256],
                  [0x00/256, 0x38/256, 0xa8/256],
                  [0,0,0],
                 ],
                # pansexual flag
                [[0xff/256, 0x1c/256, 0x8d/256],
                 [0xff/256, 0xd7/256, 0x00/256],
                 [0x1a/256, 0xb3/256, 0xff/256],
                 ],
                # nonbin
                [[0xfc/256, 0xf4/256, 0x31/256],
                 [0xfc/256, 0xfc/256, 0xfc/256],
                 [0x9d/256, 0x59/256, 0xd2/256],
                 [0x28/256, 0x28/256, 0x28/256],
                 ],
                # lesbian
                [[0xd6/256, 0x28/256, 0x00/256],
                 [0xff/256, 0x9b/256, 0x56/256],
                 [0xff/256, 0xff/256, 0xff/256],
                 [0xd4/256, 0x62/256, 0xa6/256],
                 [0xa4/256, 0x00/256, 0x62/256],
                 ],
                # agender
                [[0x00/256, 0x00/256, 0x00/256],
                 [0xba/256, 0xba/256, 0xba/256],
                 [0xff/256, 0xff/256, 0xff/256],
                 [0xba/256, 0xf4/256, 0x84/256],
                 [0xff/256, 0xff/256, 0xff/256],
                 [0xba/256, 0xba/256, 0xba/256],
                 [0x00/256, 0x00/256, 0x00/256],
                 ],
                # asex
                [[0x00/256, 0x00/256, 0x00/256],
                 [0xa4/256, 0xa4/256, 0xa4/256],
                 [0xff/256, 0xff/256, 0xff/256],
                 [0x81/256, 0x00/256, 0x81/256],
                 ],
                # genderqueer
                [[0xb5/256, 0x7f/256, 0xdd/256],
                 [0xff/256, 0xff/256, 0xff/256],
                 [0x49/256, 0x82/256, 0x1e/256],
                 ],
                # genderfluid
                [[0xfe/256, 0x76/256, 0xa2/256],
                 [0xff/256, 0xff/256, 0xff/256],
                 [0xbf/256, 0x12/256, 0xd7/256],
                 [0x00/256, 0x00/256, 0x00/256],
                 [0x30/256, 0x3c/256, 0xbe/256],
                 ],
                # aromantic
                [[0x3b/256, 0xa7/256, 0x40/256],
                 [0xa8/256, 0xd4/256, 0x7a/256],
                 [0xff/256, 0xff/256, 0xff/256],
                 [0xab/256, 0xab/256, 0xab/256],
                 [0x00/256, 0x00/256, 0x00/256],
                 ],
                ]
        self.scheme = 0

        leds.set_all_rgb(0,0,0)


    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self.t += delta_ms

        petals = self.input.captouch.petals
        btns = self.input.buttons.app

        if petals[2].whole.pressed or btns.right.pressed:
            self.scheme = (self.scheme + 1) % len(self.colors)
            self.t = 0
            self.last_led_update = -1000
        elif petals[8].whole.pressed or btns.left.pressed:
            self.scheme = (self.scheme - 1) % len(self.colors)
            self.t = 0
            self.last_led_update = -1000

        self.pos = (self.t / 20) % 240


    def draw(self, ctx: Context) -> None:
        ctx.image_smoothing = False
        nr_colors = len(self.colors[self.scheme])
        stripe = 240 / nr_colors
        idx = int(self.pos / stripe)
        yoff = self.pos % stripe

        # center of screen is [0, 0]
        # x goes right, y goes down -> bottom left is [-120, +120]

        for i in range(nr_colors+1):
            col = self.colors[self.scheme][(idx-i)%nr_colors]
            ctx.rectangle(-120, +120 - yoff - stripe*i, 240, stripe)
            ctx.rgb(col[0], col[1], col[2])
            ctx.fill()

        # we don't need to update the LEDs every few ms :)
        if not self.last_led_update or self.t - self.last_led_update > 50:
            for i in range(40):
                led_idx = int(nr_colors * (i / 40 + self.pos/240))
                leds.set_rgb(i, *self.colors[self.scheme][led_idx % nr_colors])
            leds.update()
            self.last_led_update = self.t


# For running with `mpremote run`:
if __name__ == "__main__":
    st3m.run.run_view(Flag(ApplicationContext()))
